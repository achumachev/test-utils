package org.dio.commons.testutils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.beanutils.BeanMap;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Assert;

/**
 * User: Alex.Chumachev
 * Date: 2/19/13
 */
public class POJOAssert {
    private final Map<String, Matcher> values = new HashMap<String, Matcher>();
    private final Collection<String> ignores = new HashSet<String>();

    public POJOAssert ignore(String... properties) {
        Collections.addAll(ignores, properties);

        return this;
    }

    public POJOAssert expectThat(String property, Matcher matcher) {
        values.put(property, matcher);

        return this;
    }

    public POJOAssert expectEquals(String property, Object value) {
        values.put(property, CoreMatchers.equalTo(value));

        return this;
    }

	@SuppressWarnings("unchecked")
	public void assertPOJO(Object value) {
        Collection<String> innerIgnores = new HashSet<String>(ignores);
        final BeanMap map = new BeanMap(value);
        Map<String, Object> errors = new HashMap<String, Object>();

        for (Object e : map.entrySet()) {
        	Map.Entry<String, Object> entry = (Map.Entry<String, Object>) e; 
			String property = entry.getKey();
			Object actual = entry.getValue();

            if ("class".equals(property)) {
                continue;
            }

            if (innerIgnores.contains(property)) {
                innerIgnores.remove(property);
                continue;
            } 

            boolean found = false;
            for (Map.Entry<String, Matcher> expectation : values.entrySet()) {
                if (expectation.getKey().equals(property)) {
                    Assert.assertThat(actual, expectation.getValue());
                    found = true;
                    break;
                }
            }

            if (!found) {
                errors.put(property, actual);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> e : errors.entrySet()) {
            sb.append("Property [")
                    .append(e.getKey())
                    .append("] wasn't expected and has value [")
                    .append(e.getValue())
                    .append("]. ");
        }

        if (!innerIgnores.isEmpty()) {
            sb.append(innerIgnores.size() > 1 ? "Properties " : "Property ")
                    .append(innerIgnores)
                    .append(" was declared as ignored but never used.\r\n");
        }

        if (sb.length() > 0) {
            throw new AssertionError(sb.toString());
        }
    }
}
