package org.dio.commons.testutils;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * User: Alex.Chumachev
 * Date: 12/20/13
 */
public class POJOAssertTest {
    private User user;

    @Before
    public void setUp() {
        user = new User();
        user.setAge(30);
        user.setCreationDate(new Date(123));
        user.setEmail("test@email.com");
        user.setMale(true);
        user.setName("Test");
        user.setPassword("passwd");
        user.setSalary(123.2f);
    }

    @Test
    public void testOk() {
        new POJOAssert()
                .expectEquals("age", 30)
                .expectEquals("creationDate", new Date(123))
                .expectEquals("email", "test@email.com")
                .expectEquals("male", true)
                .expectEquals("salary", 123.2f)
                .ignore("password", "name")
                .assertPOJO(user);
    }

    @Test(expected = AssertionError.class)
    public void testNotSpecified() {
        new POJOAssert()
                .expectEquals("age", 30)
                .expectEquals("creationDate", new Date(123))
                .expectEquals("email", "test@email.com")
                .expectEquals("male", true)
                .ignore("password", "name")
                .assertPOJO(user);
    }

    @Test(expected = AssertionError.class)
    public void testWrongValue() {
        new POJOAssert()
                .expectEquals("age", 30)
                .expectEquals("creationDate", new Date(123))
                .expectEquals("email", "wrong@email.com")
                .expectEquals("male", true)
                .expectEquals("salary", 123.2f)
                .ignore("password", "name")
                .assertPOJO(user);
    }

    @Test
    public void testMatchers() {
        new POJOAssert()
                .expectThat("age", CoreMatchers.any(Integer.class))
                .expectThat("creationDate", CoreMatchers.instanceOf(Date.class))
                .expectThat("email", CoreMatchers.notNullValue())
                .expectEquals("male", true)
                .expectEquals("salary", 123.2f)
                .ignore("password", "name")
                .assertPOJO(user);
    }

    @Test
    public void testMultiUsage() {
        POJOAssert pojoAssert = new POJOAssert();

        pojoAssert.expectEquals("age", 30)
                .expectEquals("creationDate", new Date(123))
                .expectEquals("email", "test@email.com")
                .expectEquals("salary", 123.2f)
                .ignore("password", "name", "male");

        User user2 = new User();
        user2.setAge(30);
        user2.setCreationDate(new Date(123));
        user2.setEmail("test@email.com");
        user2.setMale(false);
        user2.setName("Testy");
        user2.setPassword("passwd2");
        user2.setSalary(123.2f);

        pojoAssert.assertPOJO(user);
        pojoAssert.assertPOJO(user2);
    }
}
